module Main where

import Data.Either
import System.Environment

import Safe

import Raza.Parsing.BaseTypes
import Raza.Parsing.Parser
import Raza.Runtime.Interpreter
import Raza.Runtime.BaseTypes

runFile :: FilePath -> IO ()
runFile path = do
    astMaybe <- parseFile path
    let ast = fromRight (Block []) astMaybe
    result <- execRazaM (runBlock ast)
    case result of
        Right _ -> return ()
        Left err -> print err

main :: IO ()
main = do 
    args <- getArgs
    case headMay args of
        Just fn -> runFile fn
        Nothing -> putStrLn "No filename received"
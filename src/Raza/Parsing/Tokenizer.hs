module Raza.Parsing.Tokenizer where

import Data.Char

import Safe

import Raza.Parsing.BaseTypes

tokenize :: String -> Either TokenizerError [Token]
tokenize = tokenize' 1 1
    where 
        tokenize' :: Integer -> Integer -> String -> Either TokenizerError [Token]

        tokenize' row col []  = Right [Token EOFToken row col]
        tokenize' row col (c:rest) = case c of 
            ' '  -> tokenize' row (col+1) rest
            '('  -> (:) (Token LeftParens     row col) <$> tokenize' row     (col+1) rest
            ')'  -> (:) (Token RightParens    row col) <$> tokenize' row     (col+1) rest
            '['  -> (:) (Token LeftBracket    row col) <$> tokenize' row     (col+1) rest
            ']'  -> (:) (Token RightBracket   row col) <$> tokenize' row     (col+1) rest
            '{'  -> (:) (Token LeftBrace      row col) <$> tokenize' row     (col+1) rest
            '}'  -> (:) (Token RightBrace     row col) <$> tokenize' row     (col+1) rest
            ','  -> (:) (Token CommaToken     row col) <$> tokenize' row     (col+1) rest
            '+'  -> (:) (Token PlusToken      row col) <$> tokenize' row     (col+1) rest
            '-'  -> (:) (Token MinusToken     row col) <$> tokenize' row     (col+1) rest
            '*'  -> (:) (Token StarToken      row col) <$> tokenize' row     (col+1) rest
            '/'  -> (:) (Token SlashToken     row col) <$> tokenize' row     (col+1) rest
            ';'  -> (:) (Token SemicolonToken row col) <$> tokenize' row     (col+1) rest 
            '\n' -> (:) (Token NewLineToken   row col) <$> tokenize' (row+1) 1       rest

            '!' -> case headMay rest of 
                Just '=' -> (:) (Token BangEqualsToken   row col) <$> tokenize' row (col+2) (tailSafe rest)
                _        -> (:) (Token BangToken         row col) <$> tokenize' row (col+1) rest
            '=' -> case headMay rest of 
                Just '=' -> (:) (Token DoubleEqualsToken row col) <$> tokenize' row (col+2) (tailSafe rest)
                Just '>' -> (:) (Token FatArrowToken     row col) <$> tokenize' row (col+2) (tailSafe rest)
                _        -> (:) (Token EqualsToken       row col) <$> tokenize' row (col+1) rest
            '>' -> case headMay rest of 
                Just '=' -> (:) (Token GreaterEqualToken row col) <$> tokenize' row (col+2) (tailSafe rest)
                _        -> (:) (Token GreaterToken      row col) <$> tokenize' row (col+1) rest
            '<' -> case headMay rest of 
                Just '=' -> (:) (Token LessEqualToken    row col) <$> tokenize' row (col+2) (tailSafe rest)
                _        -> (:) (Token LessToken         row col) <$> tokenize' row (col+1) rest

            '"' -> do 
                (str, rest') <- tokenizeString "" rest
                restTokens <- tokenize' row (col + toInteger (length str)) rest'
                return $ Token (StringToken str) row col : restTokens
                where 
                    tokenizeString 
                        :: String -> String -> Either TokenizerError (String, String)
                    tokenizeString _ [] = Left $ TokenizerError UnterminatedString row col
                    tokenizeString acc ('"':cs) = Right (acc,cs)
                    tokenizeString acc (sc:scs) = tokenizeString (acc++[sc]) scs

            _ | isNumber c -> 
                let (numStr, rest') = span isNumber (c:rest) in
                case readMay numStr of 
                    Just number -> (:) (Token (IntToken number) row col) <$> tokenize' row (col + toInteger (length numStr)) rest'
                    Nothing -> Left $ TokenizerError (UnableToTokenize numStr) row col

            _ | isIdentifierStart c -> 
                let (word, rest') = span isIdentifier (c:rest) in 
                (:) (Token (wordToTokenType word) row col) <$> tokenize' row (col+ toInteger (length word)) rest'

            _ -> Left $ TokenizerError (UnexpectedChar c) row col

isIdentifierStart :: Char -> Bool
isIdentifierStart c = isAlpha c || c `elem` "_$"

isIdentifier :: Char -> Bool
isIdentifier c = isIdentifierStart c || isNumber c

wordToTokenType :: String -> TokenType
wordToTokenType word
    | word == "var"   = VarToken
    | word == "let"   = LetToken
    | word == "print" = PrintToken
    | word == "True"  = TrueToken
    | word == "False" = FalseToken
    | word == "Nil"   = NilToken
    | word == "if"    = IfToken
    | word == "else"  = ElseToken
    | word == "fun"   = FunToken
    | otherwise       = IdentifierToken (Name word)

splitAtElem :: Eq a => a -> [a] -> ([a], [a])
splitAtElem sep xs = case span (/=sep) xs of 
    (begin, end) -> (begin, tailSafe end)
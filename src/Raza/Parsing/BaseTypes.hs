module Raza.Parsing.BaseTypes where

import Prelude hiding (getLine)
import Data.List

import Text.Parsec.Error
import Text.Parsec.Pos

newtype Name = Name { unName :: String }
    deriving (Ord, Eq)

instance Show Name where
    show n = "{" ++ unName n ++ "}"

class HasPosition t where
    getLine :: t -> Integer
    getColumn :: t -> Integer

    getPos :: SourceName -> t -> SourcePos
    getPos fn x = newPos fn (fromInteger . getLine $ x) (fromInteger . getColumn $ x)

data TokenType = 
    -- Grouping
      LeftParens | RightParens | LeftBracket | RightBracket | LeftBrace | RightBrace
    -- Literals
    | IdentifierToken Name | StringToken String | IntToken Integer | DecimalToken Double
    -- Reserved Words
    | LetToken | VarToken | PrintToken | TrueToken | FalseToken | NilToken | IfToken 
    | ElseToken | FunToken
    -- Operators
    | EqualsToken | PlusToken | MinusToken | SlashToken | StarToken | DoubleEqualsToken 
    | BangEqualsToken | BangToken | GreaterToken | GreaterEqualToken | LessToken | LessEqualToken
    | FatArrowToken | CommaToken | SemicolonToken
    -- Other
    | NewLineToken | EOFToken
    deriving Eq

instance Show TokenType where
    show LeftParens = "("
    show RightParens = ")"
    show LeftBracket = "["
    show RightBracket = "]"
    show LeftBrace = "{"
    show RightBrace = "}"
    show (IdentifierToken name) = "Identifer " ++ show name
    show (StringToken s) = "String \"" ++ s ++ "\""
    show (IntToken num) = "Number " ++ show num 
    show (DecimalToken num) = "Number " ++ show num 
    show LetToken = "let"
    show VarToken = "var"
    show PrintToken = "print"
    show TrueToken = "true"
    show FalseToken = "false"
    show NilToken = "nil"
    show IfToken = "if"
    show ElseToken = "else"
    show FunToken = "fun"
    show EqualsToken = "="
    show PlusToken = "+"
    show MinusToken = "-"
    show SlashToken = "/"
    show StarToken = "*"
    show DoubleEqualsToken = "=="
    show BangEqualsToken = "!="
    show BangToken = "!"
    show GreaterToken = ">"
    show GreaterEqualToken = ">="
    show LessToken = "<="
    show LessEqualToken = "<="
    show FatArrowToken = "=>"
    show CommaToken = ","
    show SemicolonToken = ";"
    show NewLineToken = "\\n"
    show EOFToken = "EOF"

data Token = Token {
    tokenType   :: TokenType,
    tokenLine   :: Integer,
    tokenColumn :: Integer
} deriving Eq

instance Show Token where
    show Token {tokenType, tokenLine, tokenColumn} = show tokenType 
        ++ " at line " ++ show tokenLine 
        ++ ", column " ++ show tokenColumn

instance HasPosition Token where
    getLine = tokenLine
    getColumn = tokenColumn

data TokenizerError = TokenizerError {
    errorType   :: TokenizerErrorType,
    errorLine   :: Integer,
    errorColumn :: Integer
} deriving Show

instance HasPosition TokenizerError where
    getLine = errorLine
    getColumn = errorColumn

data TokenizerErrorType = UnterminatedString
                        | UnableToTokenize String
                        | UnexpectedChar Char
    deriving Show

toParseError :: String -> TokenizerError -> ParseError
toParseError fn err = newErrorMessage 
        (Message $ show err)
        (newPos fn (fromIntegral . getLine $ err) (fromIntegral . getColumn $ err))

newtype Block = Block [Stmt]

data Stmt = ExprStmt Expr
          | LetStmt Name Expr
          | VarStmt Name Expr
          | AssignStmt Name Expr
          | PrintStmt Expr

data BinOp = Add | Sub | Mul | Div | Equal | NotEqual | Greater | Less | LessEqual | GreaterEqual
           deriving Show

data UnaryOp = Not | Minus deriving Show

data Expr = 
    -- Literals
      StrExpr String
    | IntExpr Integer
    | DoubleExpr Double
    | Identifier Name
    | BoolExpr Bool
    | NilExpr
    -- Functions
    | FunctionExpr [Name] Block
    | CallExpr Expr [Expr]
    -- Operator Expressions    
    | BinExpr BinOp Expr Expr
    | UnaryExpr UnaryOp Expr
    -- Other
    | IfExpr Expr Block Block

instance Show Block where
    show (Block stmts) = "{\n    " ++ intercalate ";\n    " (map show stmts) ++ ";\n}"

valueShow :: String -> String -> String
valueShow name val = name ++ "[" ++ val ++ "]"

instance Show Expr where
    show (BinExpr op l r ) = show op ++ "(" ++ show l ++ ", " ++ show r ++ ")"
    show (UnaryExpr op e ) = show op ++ "(" ++ show e ++ ")"

    show (CallExpr  c  as) = "call(" ++ show c ++ ", " ++ show as ++ ")"

    show (StrExpr    s   ) = valueShow "string"     (show s)
    show (IntExpr    n   ) = valueShow "num"        (show n)
    show (DoubleExpr n   ) = valueShow "num"        (show n)
    show (Identifier i   ) = valueShow "identifier" (show i)
    show (BoolExpr   b   ) = valueShow "bool"       (show b)
    show NilExpr                     = "Nil"
    show (IfExpr cond tBlock fBlock) = "if(" ++ show cond ++ "," ++ show tBlock ++ "," ++ show fBlock ++ ")"
    show (FunctionExpr args b      ) = "fun(" ++ show args ++ ", " ++ show b ++ ")"

instance Show Stmt where
    show (ExprStmt e   ) = "ExprStmt(" ++ show e ++ ")"
    show (VarStmt    name e) = "Var(" ++ show name ++ ", " ++ show e ++ ")"
    show (LetStmt    name e) = "Let(" ++ show name ++ ", " ++ show e ++ ")"
    show (AssignStmt name e) = "Assign(" ++ show name ++ ", " ++ show e ++ ")"
    show (PrintStmt e      ) = "Print(" ++ show e ++ ")"
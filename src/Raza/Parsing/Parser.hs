module Raza.Parsing.Parser where 

import Prelude hiding (getLine)
import Data.Bifoldable()
import Control.Monad

import Text.ParserCombinators.Parsec hiding (token)
import Text.ParserCombinators.Parsec.Expr
import qualified Text.ParserCombinators.Parsec as Parsec (token)

import Raza.Parsing.BaseTypes
import Raza.Parsing.Tokenizer

type RazaParser = GenParser Token ()

identifier :: RazaParser Name
identifier = do 
    sourcePos <- getPosition
    let fn = sourceName sourcePos
    Parsec.token show (getPos fn) $ \case 
        Token (IdentifierToken name) _ _ -> Just name 
        _ -> Nothing

stringLiteral :: RazaParser String
stringLiteral = do 
    sourcePos <- getPosition
    let fn = sourceName sourcePos
    Parsec.token show (getPos fn) $ \case 
        Token (StringToken str) _ _ -> Just str 
        _ -> Nothing

integer :: RazaParser Integer
integer = do 
    sourcePos <- getPosition
    let fn = sourceName sourcePos 
    Parsec.token show (getPos fn) $ \case 
        Token (IntToken num) _ _ -> Just num 
        _ -> Nothing
    
float :: RazaParser Double
float = do 
    sourcePos <- getPosition
    let fn = sourceName sourcePos 
    Parsec.token show (getPos fn) $ \case 
        Token (DecimalToken num) _ _ -> Just num 
        _ -> Nothing

token :: TokenType -> RazaParser TokenType
token tok = do 
    sourcePos <- getPosition
    let fn = sourceName sourcePos 
    Parsec.token show (getPos fn) $ \case 
        Token t _ _ | t == tok -> Just tok 
        _ -> Nothing

parens :: RazaParser t -> RazaParser t
parens p = do 
    _      <- token LeftParens
    result <- p 
    _      <- token RightParens
    return result

braces :: RazaParser t -> RazaParser t
braces p = do
    _      <- token LeftBrace
    result <- p 
    _      <- token RightBrace
    return result

brackets :: RazaParser t -> RazaParser t
brackets p = do
    _      <- token LeftBracket
    result <- p 
    _      <- token RightBracket
    return result

commaSep :: RazaParser t -> RazaParser [t]
commaSep p = sepBy p (token CommaToken)


-- Parser

operators :: OperatorTable Token () Expr
operators = [[ Prefix (token MinusToken >> return (UnaryExpr Minus))
             , Prefix (token BangToken >> return (UnaryExpr Not))
             ] 
            ,[ Infix (token DoubleEqualsToken >> return (BinExpr Equal)) AssocLeft
             , Infix (token BangEqualsToken >> return (BinExpr NotEqual)) AssocLeft
             , Infix (token LessToken >> return (BinExpr Less)) AssocLeft
             , Infix (token GreaterToken >> return (BinExpr Greater)) AssocLeft
             , Infix (token LessEqualToken >> return (BinExpr LessEqual)) AssocLeft
             , Infix (token GreaterEqualToken >> return (BinExpr GreaterEqual)) AssocLeft
             ]
            ,[ Infix (token StarToken >> return (BinExpr Mul)) AssocLeft
             , Infix (token SlashToken >> return (BinExpr Div)) AssocLeft
             , Infix (token PlusToken >> return (BinExpr Add)) AssocLeft
             , Infix (token MinusToken >> return (BinExpr Sub)) AssocLeft
             ]]

term :: RazaParser Expr
term =  parens expression
    <|> (token FalseToken >> return (BoolExpr False))
    <|> funExpr
    <|> StrExpr <$> stringLiteral
    <|> IntExpr <$> integer
    <|> DoubleExpr <$> float
    <|> Identifier <$> identifier
    <?> "primary expression"

expression :: RazaParser Expr
expression = buildExpressionParser operators (call <|> term)
        <?> "expression"

stmt :: RazaParser Stmt
stmt = do 
    _ <- many $ token NewLineToken
    s <- letStmt <|> varStmt <|> printStmt <|> assignStmt <|> exprStmt
    _ <- stmtEnd
    return s
    <?> "statement"

block :: RazaParser Block
block = Block <$> braces (many stmt) 
    <|> (expression >>= \e -> return $ Block [ExprStmt e])
    <?> "block"

letStmt :: RazaParser Stmt
letStmt = do
    _    <- token LetToken
    name <- identifier
    _    <- token EqualsToken
    LetStmt name <$> expression
    <?> "let statement"

varStmt :: RazaParser Stmt
varStmt = do 
    _    <- token VarToken
    name <- identifier
    _    <- token EqualsToken
    VarStmt name <$> expression
    <?> "var statement"

assignStmt :: RazaParser Stmt
assignStmt = do 
    name <- try (do 
        name <- identifier 
        _    <- token EqualsToken
        return name)
    AssignStmt name <$> expression
                

printStmt :: RazaParser Stmt
printStmt = do 
    _ <- token PrintToken
    PrintStmt <$> expression
    <?> "print statement"

exprStmt :: RazaParser Stmt
exprStmt = ExprStmt <$> expression

ifExpr :: RazaParser Expr
ifExpr = do 
    _         <- token IfToken
    cond      <- expression
    ifBlock   <- block 
    _         <- token ElseToken
    elseBlock <- try block
    return $ IfExpr cond ifBlock elseBlock
    <?> "if"

funExpr :: RazaParser Expr
funExpr = do 
    _        <- token FunToken
    argNames <- parens $ commaSep identifier
    _        <- token FatArrowToken
    FunctionExpr argNames <$> block
    <?> "function expression"

call :: RazaParser Expr
call = do 
    (callee, args) <- try $ do
        callee <- term
        args   <- parens . commaSep $ expression
        return (callee, args) 
    next   <- call' []
    return $ nestCalls callee (args : next)
    <?> "call"
    where
        nestCalls :: Expr -> [[Expr]] -> Expr
        nestCalls = foldl CallExpr

        call' :: [[Expr]] -> RazaParser [[Expr]]
        call' argss = try (do
            args <- parens . commaSep $ expression
            call' (args : argss)) 
            <|> return argss

stmtEnd :: RazaParser ()
stmtEnd = void (token SemicolonToken) 
       <|> lookAhead (void (token RightBrace))
       <|> void (token EOFToken)
       <|> void (many1 $ token NewLineToken)
       <?> "statement terminator"

-- Used for testing in ghci
rightToMaybe :: Either a b -> Maybe b
rightToMaybe = either (const Nothing) Just 

parseString :: String -> String -> Either ParseError Block
parseString fn str = case tokenize str of 
    Right strTokens -> parse (Block <$> many stmt) fn strTokens
    Left err -> Left $ toParseError fn err 

parseFile :: FilePath -> IO (Either ParseError Block)
parseFile fn = parseString fn <$> readFile fn

testParser :: RazaParser a -> String -> Either ParseError a
testParser p str = case tokenize str of 
    Right ts -> parse p "" ts
    Left err -> Left $ toParseError "" err

showParse :: FilePath -> IO ()
showParse fn = either print print =<< parseFile fn


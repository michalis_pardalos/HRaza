module Raza.Runtime.Interpreter (evalExpr, runBlock) where

import Prelude hiding (lookup)

import Data.Map.Strict 
import Control.Monad.Except

import Raza.Parsing.BaseTypes 
import Raza.Runtime.BuiltIns
import Raza.Runtime.BaseTypes

-- Exports

evalExpr :: Expr -> RazaM RazaObject
evalExpr (StrExpr    str) = return $ RazaStr str
evalExpr (IntExpr    num) = return $ RazaInt num
evalExpr (DoubleExpr num) = return $ RazaDouble num
evalExpr (BoolExpr  bool) = return $ RazaBool bool
evalExpr NilExpr          = return $ RazaNil

evalExpr (BinExpr op l r) = do
    l' <- evalExpr l
    r' <- evalExpr r 
    liftOp $ evalBinOp op l' r'
evalExpr (UnaryExpr op expr) = evalExpr expr >>= liftOp . evalUnaryOp op
evalExpr (IfExpr cond expr1 expr2   ) = evalExpr cond >>= \c -> razaIf c expr1 expr2
evalExpr (FunctionExpr args body    ) = RazaFun args body . makeEnvConstant <$> getEnv
evalExpr (Identifier name) = lookup name . envToObjMap <$> getEnv >>= \case 
    Just val -> return val
    Nothing  -> throwError $ LookupError name
evalExpr (CallExpr    call_expr arg_exprs) = do
    callee <- evalExpr call_expr
    args <- mapM evalExpr arg_exprs
    razaCall callee args


runBlock :: Block -> RazaM RazaObject
runBlock (Block []           ) = return RazaNil
runBlock (Block (this : rest)) = do
    stmtResult <- case this of
        ExprStmt expr        -> evalExpr expr
        LetStmt name expr    -> evalExpr expr >>= razaLetBind name   >> return RazaNil
        VarStmt name expr    -> evalExpr expr >>= razaVarBind name   >> return RazaNil
        AssignStmt name expr -> evalExpr expr >>= razaAssign  name   >> return RazaNil
        PrintStmt expr       -> evalExpr expr >>= liftIO . razaPrint >> return RazaNil
    case rest of
        [] -> return stmtResult
        _  -> runBlock $ Block rest

-- Internal

razaIf :: RazaObject -> Block -> Block -> RazaM RazaObject
razaIf (RazaBool True) trueBlock _ = runBlock trueBlock
razaIf (RazaBool False) _ falseBlock = runBlock falseBlock
razaIf _ _ _ = throwError TypeError

razaLetBind :: Name -> RazaObject -> RazaM ()
razaLetBind name obj = lookup name <$> getEnvMap >>= \case
    Just (Variable _) -> throwError $ VariableExistsError name
    Just (Constant _) -> throwError $ ConstExistsError name
    Nothing -> setBinding name (Constant obj)

razaVarBind :: Name -> RazaObject -> RazaM ()
razaVarBind name obj = lookup name <$> getEnvMap >>= \case
    Just (Variable _) -> throwError $ VariableExistsError name
    Just (Constant _) -> throwError $ ConstExistsError name
    Nothing -> setBinding name (Variable obj)

razaAssign :: Name -> RazaObject -> RazaM ()
razaAssign name obj = lookup name <$> getEnvMap >>= \case
    Just (Constant _) -> throwError $ ConstExistsError name
    Just (Variable _) -> setBinding name (Variable obj)
    Nothing -> throwError $ AssignToEmptyError name

razaCall :: RazaObject -> [RazaObject] -> RazaM RazaObject
razaCall (RazaFun argNames body closure) args = do 
    -- Create argument environment
    argEnv <- if length argNames == length args 
        then return $ Environment (Constant <$> fromList (zip argNames args))
        else throwError ArgumentCountError

    withEnv (Environment 
            . union (innerMap closure) -- Load closure
            . union (innerMap argEnv) -- Load arguments
            . innerMap) 
        (runBlock body)

razaCall _ _ = throwError TypeError

evalBinOp :: BinOp -> RazaObject -> RazaObject -> RazaOp RazaObject
evalBinOp          Add = razaAdd
evalBinOp          Sub = razaSub
evalBinOp          Mul = razaMul
evalBinOp          Div = razaDiv
evalBinOp        Equal = razaEq
evalBinOp     NotEqual = razaNeq
evalBinOp      Greater = razaGT
evalBinOp         Less = razaLT
evalBinOp    LessEqual = razaLTE
evalBinOp GreaterEqual = razaGTE

evalUnaryOp :: UnaryOp -> RazaObject -> RazaOp RazaObject
evalUnaryOp Not = razaNot
evalUnaryOp Minus = razaNeg

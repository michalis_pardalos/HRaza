module Raza.Runtime.BaseTypes where

import Control.Monad.State
import Control.Monad.Except
import qualified Data.Map as Map

import Raza.Parsing.BaseTypes (Name, Block)

data RazaObject = RazaNil
                | RazaInt Integer
                | RazaDouble Double
                | RazaStr String
                | RazaBool Bool
                | RazaFun [Name] Block Environment

data RazaError = TypeError
               | LookupError Name
               | ConstExistsError Name
               | VariableExistsError Name
               | AssignToEmptyError Name
               | ArgumentCountError
            deriving Show

data Binding = Variable { getValue :: RazaObject }
             | Constant { getValue :: RazaObject }
            deriving Show

newtype Environment = Environment { innerMap :: Map.Map Name Binding }
    deriving Show

newtype RazaM a = RazaM {
    runRazaM :: ExceptT RazaError (StateT Environment IO) a
    } deriving (
        Functor, Applicative, Monad, MonadIO,
        MonadState Environment, MonadError RazaError
    )

newtype RazaOp a = RazaOp { getResult :: Either RazaError a}
    deriving (Functor, Applicative, Monad, MonadError RazaError)

instance Show RazaObject where
    show RazaNil           = "RazaNil"
    show (RazaInt    num ) = "RazaInt["    ++ show num  ++ "]"
    show (RazaDouble num ) = "RazaDouble[" ++ show num  ++ "]"
    show (RazaStr    str ) = "RazaStr["    ++ str       ++ "]"
    show (RazaBool   bool) = "RazaBool["   ++ show bool ++ "]"
    show (RazaFun _ _ _  ) = "RazaFun[]"

envToObjMap :: Environment -> Map.Map Name RazaObject
envToObjMap env = getValue <$> innerMap env

makeEnvConstant :: Environment -> Environment
makeEnvConstant = Environment 
                . fmap (\case 
                    Variable {getValue} -> Constant getValue
                    Constant {getValue} -> Constant getValue)   
                .innerMap

liftOp :: RazaOp a -> RazaM a
liftOp = liftEither .getResult

execRazaM :: RazaM a -> IO (Either RazaError a)
execRazaM program = evalStateT (runExceptT (runRazaM program)) (Environment Map.empty)

getEnv :: RazaM Environment
getEnv = get

putEnv :: Environment -> RazaM ()
putEnv = put

modifyEnv :: (Environment -> Environment) -> RazaM ()
modifyEnv = modify

-- |Run a raza program in a modified environment
withEnv :: (Environment -> Environment) -- |The modification
        -> RazaM a -- |The program
        -> RazaM a
withEnv f a = do 
    initial <- getEnv

    modifyEnv f 
    result <- a
    putEnv initial

    return result

getEnvMap :: RazaM (Map.Map Name Binding)
getEnvMap = gets innerMap

setBinding :: Name -> Binding -> RazaM ()
setBinding n b = modify (Environment . Map.insert n b . innerMap )

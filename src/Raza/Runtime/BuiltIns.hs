module Raza.Runtime.BuiltIns where

import Control.Monad.Except

import Raza.Runtime.BaseTypes

razaAdd :: RazaObject -> RazaObject -> RazaOp RazaObject
razaAdd (RazaInt left) (RazaInt right) = return $ RazaInt (left + right)
razaAdd (RazaDouble left) (RazaDouble right) = return $ RazaDouble (left + right)
razaAdd (RazaInt left) (RazaDouble right) = return $ RazaDouble (fromInteger left + right)
razaAdd (RazaDouble left) (RazaInt right) = return $ RazaDouble (left + fromInteger right)
razaAdd (RazaStr left) (RazaStr right) = return $ RazaStr (left ++ right)
razaAdd _ _ = throwError TypeError

razaSub :: RazaObject -> RazaObject -> RazaOp RazaObject
razaSub (RazaInt left) (RazaInt right) = return $ RazaInt (left - right)
razaSub (RazaDouble left) (RazaDouble right) = return $ RazaDouble (left - right)
razaSub (RazaInt left) (RazaDouble right) = return $ RazaDouble (fromInteger left - right)
razaSub (RazaDouble left) (RazaInt right) = return $ RazaDouble (left - fromInteger right)
razaSub _ _ = throwError TypeError

razaMul :: RazaObject -> RazaObject -> RazaOp RazaObject
razaMul (RazaInt left) (RazaInt right) = return $ RazaInt (left * right)
razaMul (RazaDouble left) (RazaDouble right) = return $ RazaDouble (left * right)
razaMul (RazaInt left) (RazaDouble right) = return $ RazaDouble (fromInteger left * right)
razaMul (RazaDouble left) (RazaInt right) = return $ RazaDouble (left * fromInteger right)
razaMul _ _ = throwError TypeError

razaDiv :: RazaObject -> RazaObject -> RazaOp RazaObject
razaDiv (RazaInt left) (RazaInt right) = return $ RazaDouble (fromIntegral left / fromIntegral right)
razaDiv (RazaDouble left) (RazaDouble right) = return $ RazaDouble (left / right)
razaDiv (RazaInt left) (RazaDouble right) = return $ RazaDouble (fromInteger left / right)
razaDiv (RazaDouble left) (RazaInt right) = return $ RazaDouble (left / fromInteger right)
razaDiv _ _ = throwError TypeError

razaEq :: RazaObject -> RazaObject -> RazaOp RazaObject
razaEq RazaNil        RazaNil        = return $ RazaBool True
razaEq (RazaInt    l) (RazaInt    r) = return $ RazaBool (l == r)
razaEq (RazaDouble l) (RazaDouble r) = return $ RazaBool (l == r)
razaEq (RazaInt    l) (RazaDouble r) = return $ RazaBool (fromIntegral l == r)
razaEq (RazaDouble l) (RazaInt    r) = return $ RazaBool (l == fromIntegral r)
razaEq (RazaStr    l) (RazaStr    r) = return $ RazaBool (l == r)
razaEq (RazaBool   l) (RazaBool   r) = return $ RazaBool (l == r)
razaEq _              _              = return $ RazaBool False

razaNeq :: RazaObject -> RazaObject -> RazaOp RazaObject
razaNeq l r = razaEq l r >>= razaNot

razaLT :: RazaObject -> RazaObject -> RazaOp RazaObject
razaLT (RazaInt    l) (RazaInt    r) = return $ RazaBool (l < r)
razaLT (RazaDouble l) (RazaDouble r) = return $ RazaBool (l < r)
razaLT (RazaInt    l) (RazaDouble r) = return $ RazaBool (fromIntegral l < r)
razaLT (RazaDouble l) (RazaInt    r) = return $ RazaBool (l < fromIntegral r)
razaLT (RazaStr    l) (RazaStr    r) = return $ RazaBool (l < r)
razaLT (RazaBool   l) (RazaBool   r) = return $ RazaBool (l < r)
razaLT _              _              = throwError TypeError

razaLTE :: RazaObject -> RazaObject -> RazaOp RazaObject
razaLTE (RazaInt    l) (RazaInt    r) = return $ RazaBool (l <= r)
razaLTE (RazaDouble l) (RazaDouble r) = return $ RazaBool (l <= r)
razaLTE (RazaInt    l) (RazaDouble r) = return $ RazaBool (fromIntegral l <= r)
razaLTE (RazaDouble l) (RazaInt    r) = return $ RazaBool (l <= fromIntegral r)
razaLTE (RazaStr    l) (RazaStr    r) = return $ RazaBool (l <= r)
razaLTE (RazaBool   l) (RazaBool   r) = return $ RazaBool (l <= r)
razaLTE _              _              = throwError TypeError

razaGT :: RazaObject -> RazaObject -> RazaOp RazaObject
razaGT l r = razaLTE l r >>= razaNot

razaGTE :: RazaObject -> RazaObject -> RazaOp RazaObject
razaGTE l r = razaLT l r >>= razaNot

razaNot :: RazaObject -> RazaOp RazaObject
razaNot (RazaBool b) = return $ RazaBool (not b)
razaNot _ = throwError TypeError

razaNeg :: RazaObject -> RazaOp RazaObject
razaNeg (RazaInt num) = return $ RazaInt (-num)
razaNeg (RazaDouble num) = return $ RazaDouble (-num)
razaNeg _ = throwError TypeError

stringify :: RazaObject -> String
stringify RazaNil           = "nil"
stringify (RazaInt    num ) = show num
stringify (RazaDouble num ) = show num
stringify (RazaStr    str ) = str
stringify (RazaBool   bool) = show bool
stringify (RazaFun _ _ _  ) = "function"

razaPrint :: RazaObject -> IO ()
razaPrint = putStrLn . stringify

# Design Decisions #

## Variables ##

* Variables (mutable) with `var` values (immutable) with `let`
* Variables can also be assigned to using `IDENTIFIER = EXPR`
* Using either `var` or `let` with a declared name is an error

## Functions ##

* Syntax: `(ARGLIST) => { BODY }`
* If the last statement is an expression that is returned, otherwise, nil
* Closures:
  * Captured variables are immutable, no matter what they were before
